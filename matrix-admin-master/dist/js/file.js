
    $(document).ready(function () {
        $("#btnSubmit2").click(function(){
            var Name = $("#Name").val();
            var Type = $("#Type").val();
            var Time = $("#Time").val();
            var Calegory =$("#Calegory").val();
            var Hotline = $("#phone-mask").val();
            var Province = $("#Province").val();
            var Status = $("#Status").val();
            var Order = $("#Ordering").val();

            if (Hotline == '') {
                swal("Notice","Please enter your Hotline !","error");
                return;
            }

            if (!isMobileVietNam(Hotline))
            {
                swal("Notice","The Hotline entered must be a Vietnamese Hotline !","error");
                return;
            }
            if (Province == '') {
                swal("Notice","Please enter your Province !","error");
                return;
            }

             if (Status == '0') {
                swal("Notice","Please choose your Status !","error");
                return;
            }
            if (Order == '') {
                swal("Notice","Please enter your Order !","error");
                return;
            }

            if (!isNum(Order) || Order <=0)
            {
                swal("Notice","Order must be a positive integer !","error");
                return;
            }

            swal({  
              title: "Success!",
              text: "You clicked the button!",
              icon: "success",
              button: "OK",
            });
            
        });

        $("#btnSubmit4").click(function(){
            var Name = $("#Name").val();
            var Type = $("#Type").val();
            var Time = $("#Time").val();
            var Length = $("#Length").val();
            var Calegory =$("#Calegory").val();
            var Status = $("#Status").val();
            var Order = $("#Ordering").val();
            if (Name == '')
            {
                swal("Notice","Please enter your Destination Name !","error");
                return;
            }
            if (Type == '')
            {
                swal("Notice","Please enter your Type !","error");
                return;
            }

            if (!isEmail(Type))
            {
                swal("Notice","Type must be the Email field!","error");
                return;
            }

             if (Time == '')
            {
                swal("Notice","Please enter your Time !","error");
                return;
            }

            if (Length == '')
            {
                swal("Notice","Please enter your Length !","error");
                return;
            }

            if (Length.length > 8)
            {
                swal("Notice","The Length must not exceed 8 characters !","error");
                return;
            }

            // if (!isType(Type))
            // {
            //     swal("Notice","HEHE  !","error");
            //     return;
            // }

            if (Calegory == '0') {
                swal("Notice","Please choose your Calegory Schedule !","error");
                return;
            }
             if (Status == '0') {
                swal("Notice","Please choose your Status !","error");
                return;
            }
            if (Order == '') {
                swal("Notice","Please enter your Order !","error");
                return;
            }

            if (!isNum(Order) || Order <=0)
            {
                swal("Notice","Order must be a positive integer !","error");
                return;
            }

            swal({  
              title: "Success!",
              text: "You clicked the button!",
              icon: "success",
              button: "OK",
            });
      
        });
        $("#btnSubmit6").click(function(){
            var Schedule =$("#Schedule").val();
            var Price =$("#Price").val();
            var StartTime =$("#StartTime").val();
            var Status = $("#Status").val();
            var Order = $("#Ordering").val();

            if (Schedule == '0') {
                swal("Notice","Please choose your Schedule !","error");
                return;
            }

            if (Price == '') {
                swal("Notice","Please enter your Price !","error");
                return;
            }

            if (!isNum(Price) || Price <=0)
            {
                swal("Notice","Price must be a positive integer !","error");
                return;
            }
            if (StartTime == '')
            {
                swal("Notice","Please enter your Start Time !","error");
                return;
            }
            if (Status == '0') {
                swal("Notice","Please choose your Status !","error");
                return;
            }
             if (Order == '') {
                swal("Notice","Please enter your Order !","error");
                return;
            }

            if (!isNum(Order) || Order <=0)
            {
                swal("Notice","Order must be a positive integer !","error");
                return;
            }

            swal({  
              title: "Success!",
              text: "You clicked the button!",
              icon: "success",
              button: "OK",
            });
        });   
        function generateCode(length) {
            var result           = [];
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
              result.push(characters.charAt(Math.floor(Math.random() * 
         charactersLength)));
        }
        return result.join('');
        }
        function isNum(num)
        {
            var regex = /^\d+$/;
            return regex.test(num)
        }
        function isEmail(email)
        {
            var regex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
            return regex.test(email)
        }
        function isMobileVietNam(mobile)
        {
            var regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
            return regex.test(mobile);
        }
        // function isType(type)
        // {
        //     // var regex = /.{,8}/;
        //     var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{,8}$/;
        //      // .{1,8}
        //     return regex.test(Type);
        // }

    });
        // $(document).ready(function () {
        //     $("#btnSubmit4").click(function(){
        //         var Name = $("#Name").val();
        //         var Type = $("#Type").val();
        //         var Time = $("#Time").val();
        //         var status = $("#Status").val();
        //         var order = $("#ordering").val();
        //         if (Name == '')
        //         {
        //             alert("Mời bạn nhập vào tên");
        //             return;
        //         }
        //         if (Type == '')
        //         {
        //             alert("Mời bạn nhập vào kiểu");
        //             return;
        //         }
        //         if (Time == '')
        //         {
        //             alert("Mời bạn nhập vào thời gian");
        //             return;
        //         }
        //         if (Calegory == '0') {
        //             alert("Mời bạn chọn vào tỉnh thành");
        //             return;
        //         }
        //          if (status == '0') {
        //             alert("Mời bạn chọn vào hoạt động");
        //             return;
        //         }
        //         if (order == '0') {
        //             alert("Mời bạn nhập vào order");
        //             return;
        //         }
                
        //         // $("#resName").value = "abc";
        //         // document.getElementById("resName").innerHTML="abc";
        //     });
        // });
       